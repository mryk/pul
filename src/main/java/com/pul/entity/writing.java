package com.pul.entity;

import java.util.Date;

public class writing {
    private int writingID;
    private String writing;
    private String account;
    private String type;
    private Date time;

    public String getWriting() {
        return writing;
    }

    public void setWriting(String writing) {
        this.writing = writing;
    }

    public int getWritingID() {
        return writingID;
    }

    public void setWritingID(int writingID) {
        this.writingID = writingID;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
