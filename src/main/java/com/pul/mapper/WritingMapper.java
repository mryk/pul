package com.pul.mapper;

import com.pul.entity.writing;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface WritingMapper {

    //发布文案
    @Insert("insert into writing values(0,#{writing},#{account},#{type},#{time})")
    void release(writing writing);


    //编辑文案
    @Update("update writing,type from writing where account=#{account}")
    void editwriting(writing writing);

    //查找文案
    @Select("select * from writing where account=#{account}")
    void searchwriting(writing writing);

    //查找所有文案
    @Select("select writing from writing")
    String[] searchAllWriting();
}
