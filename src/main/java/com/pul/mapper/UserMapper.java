package com.pul.mapper;

import com.pul.entity.user;
import com.pul.entity.systemmanger;
import com.pul.entity.writing;
import org.apache.ibatis.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Mapper
@Repository
public interface UserMapper {
    //根据账号查找用户
    @Select("select * from user where account=#{account}")
    user findByaccount(String account);

    //注册
    @Insert("insert into user(account,password,username,gender,autograph,like1,like2,like3) values(#{account},#{password},#{username},#{gender},#{autograph},#{like1},#{like2},#{like3})")
    void register(user user);

    //修改密码
    @Update("update user set password=#{password} where account=#{account}")
    void upadtePassword(String password);

    //匹配用户账号密码
    @Select("select * from user where account=#{account} and password=#{password}")
    user login_user(user user);

    //修改基本信息
    @Update("update username,gender,autograph from user where account=#{account}")
    void updateDetial(user user);



}
