package com.pul.mapper;

import com.pul.entity.user;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface SystemmangerMapper {

    //封号
    @Delete("delete from user where account=#{account}")
    void delete(user user);

    //删除文案
    @Delete("delete from writing where account=#{account}")
    void deleteWriting(user user);


}
