package com.pul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PulApplication {

    public static void main(String[] args) {
        SpringApplication.run(PulApplication.class, args);
    }

}
