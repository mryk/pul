package com.pul.controller;

import com.pul.entity.user;
import com.pul.service.userService;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.util.Random;


@Controller
@RequestMapping("userDetails")
public class UserController {
    private static  final Logger log= LoggerFactory.getLogger(UserController.class);
    @Autowired
    private userService userService;
    private user user;
    public String Account(){  //生成九位随机账号
        int[] accountArry=new int[9];
        StringBuffer accountnum=new StringBuffer();
        for(int i=0;i<9;i++){
            accountArry[i]=new Random().nextInt(9);
            accountnum.append(accountArry[i]);
        }
        String account= accountnum.toString();
        return account;
    }

    @RequestMapping("/src/register")
    public String register(user user) throws UnsupportedEncodingException {      //用户注册
        log.debug("昵称:{}, 性别:{}, 密码:{}, 个性签名:{},偏好1:{}, 偏好2:{}, 偏好3{}",user.getUsername(),user.getGender(),user.getPassword(),user.getAutograph(),user.getLike1(),user.getLike2(),user.getLike3());
        String account=Account();
        do{      //初始化用户账号，直至账号唯一
            user.setAccount(Account());
        }while (user.getAccount().equals(userService.findByaccount(account).getAccount()));

        Class cs=user.getClass();
//        boolean flag=false;
        for(Field f : cs.getDeclaredFields()){//通过反射检查对象属性是否有空值
            f.setAccessible(true);            //关闭反射对象安全性检查
                try {
                    f.get(user).equals(null); throw new RuntimeException("请将信息填写完整");
                } catch (IllegalAccessException e) {
                    e.printStackTrace();   //抛出异常
                    return "/src/register"+ URLEncoder.encode(e.getMessage(),"UTF-8");
                }
        }
        userService.register(user);   //所有检查完毕，完成注册
        return "/public/index";     //重定向至登录页面
    }

    @RequestMapping("/public/index")
    public String login_user(user user) throws UnsupportedEncodingException {  //用户登录
        log.debug("账号:{}, 密码:{}",user.getAccount(),user.getPassword());
        try {
            if (userService.login_user(user).equals(null))
                throw new RuntimeException("账号或密码错误");
            else
                return "/src/userIndex";
        }
        catch (RuntimeException e){
                return "/src/index"+URLEncoder.encode(e.getMessage(),"UTF-8");
        }

    }

    @RequestMapping("/src/userDetails")
    public String findUserDetails(String account, Model model){  //查找用户详细信息,返回用户详细信息页面
        //根据account查询用户
        log.debug("账号:{}",account);
        model.addAttribute("user",userService.findByaccount(account));
        return "/src/userDetails";
    }

    public user findUser(String account){  //查找用户详细信息,返回用户
        //根据account查询用户
        log.debug("账号:{}",account);
        return user;
    }
    @RequestMapping("updateUserData")
    public String UpdateUserData(user user){
        //更新用户信息
        log.debug("账号:{}",user.getAccount());
        log.debug("昵称:{}",user.getUsername());
        log.debug("性别:{}",user.getGender());
        log.debug("个性签名:{}",user.getAutograph());
        userService.updateDetial(user);
        return "/src/userDetails";
    }
    //@RequestMapping("updateUserData")
    public String UpdatePassword(user user) throws UnsupportedEncodingException {
        log.debug("账号:{}",user.getAccount());
        log.debug("旧密码:{}",user.getPassword());
        try {
            if (!user.getPassword().equals(findUser(user.getAccount()).getPassword()))
            throw new RuntimeException("输入旧密码有误");
            else{
                log.debug("新密码:{}", user.getPassword());
                userService.updatePassword(user.getPassword());
            }
        }
        catch (RuntimeException e){
             return "/src/updataUserData"+URLEncoder.encode(e.getMessage(),"UTF-8");
        }
        return "/src/userDetials";
    }

}
