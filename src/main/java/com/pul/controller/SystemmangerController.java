package com.pul.controller;

import com.pul.entity.user;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.pul.service.SystemmangerService;
import com.pul.service.userService;

@Controller
@RequestMapping("/src/systemmanger")
public class SystemmangerController {
    @Autowired
    SystemmangerService systemmangerService;
    @Autowired
    userService userService;
    private static  final Logger log= LoggerFactory.getLogger(UserController.class);
    private user user;
    @RequestMapping("/src/systemmanger")
    public String delete(user user,String account){  //封号
        systemmangerService.delete(userService.findByaccount(account));
        return "/src/systemmanger";
    }

    //@RequestMapping("/src/systemmanger")
    public String deleteWriting(user user,String account){  //删帖

        systemmangerService.deleteWriting(userService.findByaccount(account));
        return "/src/systemmanger";
    }

}
