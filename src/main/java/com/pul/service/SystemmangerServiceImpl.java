package com.pul.service;

import com.pul.entity.user;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.pul.mapper.SystemmangerMapper;

@Service
@Transactional
public class SystemmangerServiceImpl implements SystemmangerService {

    @Autowired
    SystemmangerMapper systemmangerMapper;
    @Override
    public void delete(user user){
        systemmangerMapper.delete(user);
    }

    @Override
    public void deleteWriting(user user){
        systemmangerMapper.delete(user);
    }
}
