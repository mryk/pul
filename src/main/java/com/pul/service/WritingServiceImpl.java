package com.pul.service;

import com.pul.entity.writing;
import com.pul.mapper.WritingMapper;
import org.springframework.beans.factory.annotation.Autowired;

public class WritingServiceImpl implements WritingService{

    @Autowired
    WritingMapper writingMapper;
    @Override
    public void release(writing writing){
        writingMapper.release(writing);
    }

    @Override
    public String[] searchAllWriting(){
        return writingMapper.searchAllWriting();
    }
}
