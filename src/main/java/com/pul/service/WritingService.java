package com.pul.service;

import com.pul.entity.writing;

public interface WritingService {

    //发布文案
    void release(writing writing);

    //String数组打印所有文案
    String[] searchAllWriting();
}
