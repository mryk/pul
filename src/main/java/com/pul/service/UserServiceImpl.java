package com.pul.service;

import com.pul.entity.user;
import com.pul.entity.writing;
import com.pul.mapper.UserMapper;
import com.pul.mapper.WritingMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

@Service
@Transactional
public class UserServiceImpl implements userService{
    @Autowired
    UserMapper userMapper;

    @Autowired
    WritingMapper writingMapper;
    @Override
    public user findByaccount(String account) {
         return userMapper.findByaccount(account);
    }

    @Override
    public void register(user user) {
         userMapper.register(user);
    }

    @Override
    public user login_user(user user){
        return userMapper.login_user(user);
    }

    @Override
    public void updateDetial(user user){
        userMapper.updateDetial(user);
    }

    @Override
    public void updatePassword(String password){
        userMapper.upadtePassword(password);
    }

    @Override
    public void editwriting(writing writing){
        writingMapper.editwriting(writing);
    }

    @Override
    public void searchwriting(writing writing){
        writingMapper.searchwriting(writing);
    }

}
