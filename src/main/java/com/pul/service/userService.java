package com.pul.service;

import com.pul.entity.user;
import com.pul.entity.writing;

public interface userService {
    //根据账号查找用户
    user findByaccount(String account);

    //修改密码
    void updatePassword(String password);

    //用户注册
    void register(user user);

    //用户登录
    user login_user(user user);

    //修改用户基本信息
    void updateDetial(user user);

    //编辑文案
    void editwriting(writing writing);

    //根据用户查找文案
    void searchwriting(writing writing);

}
